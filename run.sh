#!/bin/bash
# one script to rule them all

set +o noglob

#####
# (1) configuration

# (1a) catalogs and input masks to be present in data/

# tracer catalog - specify your file path 
tracers="sim/sim_m_500_5000000.fits" 
# FITS column names for RA, DEC, z coordinates
tracers_racol="RA"
tracers_deccol="DEC"
tracers_zcol="ZREDMAGIC"

# tracer precursor healpix map - specify your file path
mask="sim/MASK_FULL_SKY.fits"
# FITS column names for HPIX, fracgood, z_max
mask_hpixcol="HPIX"
mask_fracgood="FRACGOOD"
mask_zmax="LIMMAG" # always above 20, but the SDSS mask has no spatially varying zmax anyway (it's 0.45)
mask_nside=2048

# (1b) setting for trough catalog

# count config
countradii="10 20 30"     # radii (arcmin) used

countredshift_min=( "0.2" )
countredshift_max=( "0.4" )
jackkniferedshift_max="0.4"  #just assumed it
npatch=100

countpercentiles="0.2 0.4 0.6 0.8"  # lower percentile for trough selection

count_maxmaskfrac="0.1" 

# (1d) settings for this script
base=`pwd` # sim directory

trough_nside=2048


# then call do_it_all_template.sh with these settings
#### This one creates a healpix map of what fraction of each pixel was observed by the galaxy survey
##########################
 echo "(1.1) convert precursor mask to healpix mask"
 #mkdir -p lenses
 #cd sim

 if [ "$incremental" == "0" ]
 then
   rm -f mask_zcut_sim_*.fits
 fi
 for i in "${countredshift_max[@]}"
 do
   if [ ! -f mask_zcut_sim_$i.fits ]
   then
      echo $sim python3 mkhealpixmask_sim.py $mask mask_zcut_sim_$i.fits $i ${mask_hpixcol} ${mask_fracgood} ${mask_zmax} ${mask_nside} ${sysweight}
      $sim python3 mkhealpixmask_sim.py $mask mask_zcut_sim_$i.fits $i ${mask_hpixcol} ${mask_fracgood} ${mask_zmax} ${mask_nside} ${sysweight}
   fi
 done
 cd ..

#########################
### Just converting a FITS catalog to a text file to be read by the following step
##########################
 echo "(1.2) convert tracer catalog to txt file"
 cd sim
 for(( i=0; i<${#countredshift_min[@]}; ++i ))
 do
   lowz=${countredshift_min[$i]}
   highz=${countredshift_max[$i]}
   if [ "$incremental" == "0" ]
   then
     rm -f tracers_sim_${lowz}_${highz}.txt
   fi

   if [ ! -f tracers_sim_${lowz}_${highz}.txt ]
   then
     echo $sim python3 cut_mask_z_sim.py $tracers $tracers_racol $tracers_deccol $tracers_zcol $lowz $highz mask_zcut_sim_highz.fits
     $sim python3 cut_mask_z_sim.py $tracers $tracers_racol $tracers_deccol $tracers_zcol $lowz $highz mask_zcut_sim_$highz.fits > tracers_sim_${lowz}_${highz}.txt
   fi
 done

 cd ..

 ### count_in_trough: should port that to python
##########################
 echo "(1.3) convert tracer catalog to healpix map"
 cd sim
 for(( i=0; i<${#countredshift_min[@]}; ++i ))
 do
   lowz=${countredshift_min[$i]}
   highz=${countredshift_max[$i]}

   if [ "$incremental" == "0" ]
   then
     rm -f tracers_sim_${lowz}_${highz}.fits
   fi

   if [ ! -f tracers_sim_${lowz}_${highz}.fits ]
   then
      echo $sim python3 count_in_trough_sim.py tracers_sim_${lowz}_${highz}.txt 0 tracers_sim_${lowz}_${highz}.fits # this is with 0 radius, so just turning the catalog into a map of galaxies per pixel ... not sure we need this in this project
      $sim python3 count_in_trough_sim.py tracers_sim_${lowz}_${highz}.txt 0 tracers_sim_${lowz}_${highz}.fits
   fi
 done

 cd ..
##########################
if [ "$do_counts" == "1" ]
then

### count_in_trough: should port that to python
##########################
 echo "(2.1) generate count healpix map from tracer catalog"
 cd sim
 for theta in $countradii
 do
  for(( i=0; i<${#countredshift_min[@]}; ++i ))
  do
    lowz=${countredshift_min[$i]}
    highz=${countredshift_max[$i]}

    if [ "$incremental" == "0" ]
    then
      rm -f trough_sim_${theta}_${lowz}_${highz}_count.fits
    fi

    if [ ! -f trough_sim_${theta}_${lowz}_${highz}_count.fits ]
    then
      echo $sim python3 count_in_trough_sim.py tracers_sim_${lowz}_${highz}.txt $theta trough_sim_${theta}_${lowz}_${highz}_count.fits
      $sim python3 count_in_trough_sim.py tracers_sim_${lowz}_${highz}.txt $theta trough_sim_${theta}_${lowz}_${highz}_count.fits
    fi
  done
 done
 cd ..
##########################
fi
### mask_trough: what is the average fraction of area observed in a circle around each pixel; should port that to python
##########################
 echo "(2.2) generate trough-averaged z-cut mask healpix map"
 cd sim
 for theta in $countradii
 do
  if [ "$incremental" == "0" ]
  then
    rm -f trough_sim_${theta}_*_mask.fits
  fi

  for(( i=0; i<${#countredshift_min[@]}; ++i ))
  do
    highz=${countredshift_max[$i]}

    if [ ! -f trough_sim_${theta}_${highz}_mask.fits ]
    then
      echo $sim python3 mask_trough_sim.py mask_zcut_sim_$highz.fits $theta trough_sim_${theta}_${highz}_mask.fits ${trough_nside}
      $help python3 mask_trough_sim.py mask_zcut_sim_${highz}.fits $theta trough_sim_${theta}_${highz}_mask.fits ${trough_nside}
    fi
  done
 done
 cd ..
##########################
# troughfinder - should port that to python
##########################
 echo "(2.3) generate trough maps"
 cd sim
 for theta in $countradii
 do
  for(( i=0; i<${#countredshift_min[@]}; ++i ))
  do
    lowz=${countredshift_min[$i]}
    highz=${countredshift_max[$i]}

    if [ "$incremental" == "0" ]
    then
     rm -f trough_sim_${theta}_${lowz}_${highz}_map*.fits
    fi

    if [ ! -f trough_sim_${theta}_${lowz}_${highz}_0.fits ]
    then
      echo $sim python3 troughfinder_sim.py trough_sim_${theta}_${lowz}_${highz}_count.fits trough_sim_${theta}_${highz}_mask.fits ${count_maxmaskfrac} trough_sim_${theta}_${lowz}_${highz} $countpercentiles
      $sim python3 troughfinder_sim.py trough_sim_${theta}_${lowz}_${highz}_count.fits trough_sim_${theta}_${highz}_mask.fits ${count_maxmaskfrac} trough_sim_${theta}_${lowz}_${highz} $countpercentiles &> trough_sim_${theta}_${lowz}_${highz}.log
    fi
  done
 done
 cd ..
