import numpy as np
import matplotlib.pyplot as plt
from astropy.table import Table
import healpy as hp
import treecorr
from astropy.io import fits
import os
import seaborn as sns
import sys
import pandas as pd

def save_corr(cc):
    cmb_keys = list(cc.keys())
    
    cmb_values = np.zeros((len(cmb_keys)*3,20))
    for i in range(len(cmb_keys)):
        for j in range(3):
            cmb_values[3*i+j] = cc[cmb_keys[i]][j]
            
    cmb_cov = np.zeros((len(cmb_keys)*20,20))
    for c in range(len(cmb_keys)):
        for ind in range(20):
            cmb_cov[20*c+ind] = cc[cmb_keys[c]][3][ind]
            
    cmb_keys = np.asarray([cmb_keys])
            
    np.savetxt(f'corr_dict_keys_{arcmin}_{z_min}_{z_max}_{percentile}.txt', cmb_keys, delimiter=',', fmt='%s')
    np.savetxt(f'corr_dict_values_{arcmin}_{z_min}_{z_max}_{percentile}.txt', cmb_values, delimiter=',')
    np.savetxt(f'corr_dict_cov_{arcmin}_{z_min}_{z_max}_{percentile}.txt', cmb_cov, delimiter=',')
    
def load_corr():
    cc = {}
    
    cmb_keys = list(np.loadtxt(f'corr_dict_keys_{arcmin}_{z_min}_{z_max}_{percentile}.txt', delimiter=',', dtype =int))
    cmb_values = np.loadtxt(f'corr_dict_values_{arcmin}_{z_min}_{z_max}_{percentile}.txt', delimiter=',')
    cmb_cov = np.loadtxt(f'corr_dict_cov_{arcmin}_{z_min}_{z_max}_{percentile}.txt', delimiter=',')
    
    cmb_values_split = np.array_split(cmb_values, len(cmb_keys), axis=0)
    cmb_cov_split = np.array_split(cmb_cov, len(cmb_keys), axis=0)
    
    for k in range(len(cmb_keys)):
        elem = [
            cmb_values_split[k][0],
            cmb_values_split[k][1],
            cmb_values_split[k][2],
            cmb_cov_split[k]
        ]
        
        #cc[cmb_keys[k]] = np.array_split(cmb_values_split[k], 3, axis=0)
        cc[cmb_keys[k]] = elem
    
    return cc

def str_to_list(s):
    l = [float(x) for x in s.split("_")]
    return l

def list_to_str(l):
    s = f"{int(l[0])}_{float(l[1])}_{float(l[2])}_{int(l[3])}"
    return s

def save_snr(snr):
    snr_keys = list(snr.keys())
    
    snr_values = np.zeros((len(snr_keys),25))
    
    for i in range(len(snr_keys)):
        snr_values[i][:4] = str_to_list(snr_keys[i])
        snr_values[i][4] = snr[snr_keys[i]][0]
        snr_values[i][5:] = snr[snr_keys[i]][1]
                            
    np.savetxt(f'snr_dict.txt', snr_values, delimiter=',')
    
def load_snr():
    snr = {}
    
    snr_values = np.loadtxt(f'snr_dict.txt', delimiter=',')
    
    if type(snr_values[:][0]) is not np.float64:
        snr_values_split = np.array_split(snr_values, len(snr_values), axis=0)
    else : 
        snr_values_split = np.zeros((1,25))
        snr_values_split[0] = snr_values
        
    for k in range(len(snr_values_split)):
        #key = snr_values_split[k][:4]
        #key_str = list_to_str(key)
        
        val = list(snr_values_split[k][0])
        key = val[:4]
        key_str = list_to_str(key)
        
        elem = [
            val[4],
            val[5:],
        ]
        
        snr[key_str] = elem
    
    return snr

# Definition einer Funktion für die Kovarianzmatrix
def cov(x,mu):
    cov_matrix = np.zeros((20,20))
    
    for i in range(20):
        for j in range(20):
            cov_matrix[i][j] = (x[i]-mu[i]) * (x[j]-mu[j])
            
    return cov_matrix


isw_sim = hp.read_map("isw_sim.fits")
nside = hp.get_nside(isw_sim)
npix = hp.nside2npix(nside)
pixel_indices = list(range(npix))

theta, phi = hp.pix2ang(nside, pixel_indices)

dec = -np.degrees(theta-np.pi/2.)
ra = np.degrees(np.pi*2.-phi)

arcmin_list = ["10", "20", "30", "40", "50", "60", "70", "10", "35", "40", "45", "10", "10", "10", "10", "10", "10", "10", "10", "30", "10", "20", "30", "15"]
z_min_list = ["0.2", "0.2", "0.2", "0.2", "0.2", "0.2", "0.2", "0.3", "0.3", "0.3", "0.3", "0.0", "0.1", "0.1", "0.25", "0.4", "0.0", "0.5", "0.2", "0.3", "0.2", "0.2", "0.2", "0.2"]
z_max_list = ["0.4", "0.4", "0.4", "0.4", "0.4", "0.4", "0.4", "0.5", "0.5", "0.5", "0.5", "0.2", "0.3", "0.5", "0.35", "0.6", "0.6", "0.7", "0.6", "0.5", "0.5", "0.6", "0.6", "0.6"]

for num_tracer in range(len(arcmin_list)):
    print((num_tracer+1),"/",(len(arcmin_list)))
    
    
    arcmin = arcmin_list[num_tracer]
    z_min = z_min_list[num_tracer]
    z_max = z_max_list[num_tracer]
    percentile = "0"
    
    print(f"calculating : {arcmin}_{z_min}_{z_max}_{percentile}")
    
    if os.path.isfile(f"./corr_matrix_{arcmin}_{z_min}_{z_max}_{percentile}.png") :
        print("already existing")
        continue
        
    print("started")
        
    trough_sim = hp.read_map(sys.path[0] +  f"/../sim/trough_sim_{arcmin}_{z_min}_{z_max}_{percentile}.fits")
    trough_sim = hp.ud_grade(trough_sim, 1024)
    
    # ohne Noise
    # Catalogs für through-isw
    cat_isw = treecorr.Catalog(k=isw_sim, ra=ra, dec=dec, ra_units='deg', dec_units='deg')
    cat_trough = treecorr.Catalog(k=trough_sim, ra=ra, dec=dec, ra_units='deg', dec_units='deg')

    # kappa-kappa correlation trough-isw
    kk_t_i = treecorr.KKCorrelation(min_sep=10, max_sep=1200, nbins =20, sep_units='arcmin')
    kk_t_i.process(cat_isw, cat_trough)
    
    # calculating correlation function trough-isw
    xi_t_i = kk_t_i.xi
    varxi_t_i = kk_t_i.varxi
    r_t_i = np.exp(kk_t_i.meanlogr)

    corr_without_noise = [
        xi_t_i, 
        varxi_t_i,
        r_t_i
    ]
    
    
    
    
    
    num_cmb = 100

    
    # Dictionary um einzelne Korreltaionen zu speichern um danach den Mittelwert bilden zu können
    corr = {}

    # bereits vorhandene Daten laden
    if os.path.exists(f"corr_dict_values_{arcmin}_{z_min}_{z_max}_{percentile}.txt"):
        if os.path.exists(f"corr_dict_keys_{arcmin}_{z_min}_{z_max}_{percentile}.txt"):
            if os.path.exists(f"corr_dict_cov_{arcmin}_{z_min}_{z_max}_{percentile}.txt"):
                corr = load_corr()     

    # Korrelation  : through und isw + cmb(Noise)
    for i in list(range(num_cmb)):
    
        if i in corr.keys():
            continue
            
        print("Cmb-number : ",i)
        
        # einlesen der entsprechenden cmb Karte
        cmb_sim = hp.read_map(f"cmb_sim_{i}.fits")

        # Catalogs für through-isw+cmb(Noise) 
        cat_isw_cmb = treecorr.Catalog(k=((cmb_sim * 10**(6)) + isw_sim), ra=ra, dec=dec, ra_units='deg', dec_units='deg')
        cat_trough = treecorr.Catalog(k=trough_sim, ra=ra, dec=dec, ra_units='deg', dec_units='deg')

        # kappa-kappa correlation trough-isw+cmb
        kk_t_ic = treecorr.KKCorrelation(min_sep=10, max_sep=1200, nbins =20, sep_units='arcmin')
        kk_t_ic.process(cat_isw_cmb, cat_trough)
    
        # calculating correlation function trough-isw+cmb
        xi_t_ic = kk_t_ic.xi
        varxi_t_ic = kk_t_ic.varxi
        r_t_ic = np.exp(kk_t_ic.meanlogr)
    
        cov_mat = np.zeros((20,20))
    
        corr[i] = [
            xi_t_ic, 
            varxi_t_ic,
            r_t_ic,
            cov_mat
        ]
        
        if ((i%10) == 0):
            save_corr(corr)
        
    # speichern der Korrelationen
    save_corr(corr)
    
    
    
    
    
    # Mitteln der x-Werte
    x_values = np.zeros((num_cmb,20))

    for num in range(num_cmb):
        x_values[num] = corr[num][2]
    
    x_values_ave = np.average(x_values, axis=0)


    # Mitteln der y-Fehler-Werte
    y_values = np.zeros((num_cmb,20))

    for num in range(num_cmb):
        y_values[num] = corr[num][0]

    y_values_err = np.std(y_values, axis=0)
    y_values_ave = np.average(y_values, axis=0)
    
    
    
    
    # gemittelt ohne Fehler
    fig = plt.figure()
    plt.plot(
        x_values_ave,
        y_values_ave,
        label='Korrelation through und isw + random cmb (noise)'
    )

    plt.text(
        0, 
        y_values_ave[-4], 
        f"number of cmbs : N = {num_cmb} \n"+f"z $\in$ [{z_min},{z_max}] \n" + r"$\theta$ = "+f"{arcmin} arcmin"
    )
    plt.title("Korrelation through und isw + random cmb (noise)")
    plt.legend(loc='lower right', fontsize='small')
    plt.xlabel(r'$\theta$ [arcmin]')
    plt.ylabel(r'Korrelation $\xi (\theta )$')

    fig.savefig(f"corr_{arcmin}_{z_min}_{z_max}_{percentile}.png")
    
    
    
    
    
    # gemittelt mit Fehler
    fig = plt.figure()
    plt.errorbar(
        x_values_ave,
        y_values_ave,
        yerr = np.abs(y_values_err),
        label='Korrelation through und isw + random cmb (noise)'
    )

    plt.text(
        0, 
        y_values_ave[-4], 
        f"number of cmbs : N = {num_cmb} \n"+f"z $\in$ [{z_min},{z_max}] \n" + r"$\theta$ = "+f"{arcmin} arcmin"
    )

    plt.title("Korrelation through und isw + random cmb (noise)")
    plt.legend(loc='lower right', fontsize='small')
    plt.xlabel(r'$\theta$ [arcmin]')
    plt.ylabel(r'Korrelation $\xi (\theta )$')

    fig.savefig(f"corr_with_error_{arcmin}_{z_min}_{z_max}_{percentile}.png")
    
    
    
    
    
    # einzeln ohne Fehler
    fig = plt.figure()
    
    for i in range(num_cmb):
        plt.plot(
            x_values[i],
            y_values[i],
            #label= f'cmb {i}'
        )
    
    plt.text(
        0, 
        y_values_ave[-4], 
        f"number of cmbs : N = {num_cmb} \n"+f"z $\in$ [{z_min},{z_max}] \n" + r"$\theta$ = "+f"{arcmin} arcmin"
    )
    #plt.legend(loc='lower right', fontsize='small')
    plt.xlabel(r'$\theta$ [arcmin]')
    plt.ylabel(r'Korrelation $\xi (\theta )$')

    fig.savefig(f"corr_single_{arcmin}_{z_min}_{z_max}_{percentile}.png")

    
    
    
    # einzeln mit Fehler
    fig = plt.figure()
    for i in range(num_cmb):
        plt.errorbar(
            x_values[i],
            y_values[i],
            yerr = np.abs(y_values_err),
            #label= f'cmb {i}'
        )
    
    plt.text(
        0, 
        y_values_ave[-4], 
        f"number of cmbs : N = {num_cmb} \n"+f"z $\in$ [{z_min},{z_max}] \n" + r"$\theta$ = "+f"{arcmin} arcmin"
    )
    #plt.legend(loc='lower right', fontsize='small')
    plt.xlabel(r'$\theta$ [arcmin]')
    plt.ylabel(r'Korrelation $\xi (\theta )$')

    fig.savefig(f"corr_single_with_error_{arcmin}_{z_min}_{z_max}_{percentile}.png")
    
    
    
    # Anderson-Hartlap Faktor
    N = num_cmb
    N_bin = 20

    alpha = (N - N_bin - 2) / (N - 1)
    
    
    # gemittelt mit Fehler
    fig = plt.figure()
    plt.errorbar(
        x_values_ave,
        y_values_ave,
        yerr = np.abs(y_values_err),
        label='Mittelwert Korrelation through und isw + random cmb (noise)'
    )

    plt.errorbar(
        x_values_ave,
        corr_without_noise[0],
        yerr = np.abs(y_values_err),
        label='Korrelation through und isw'
    )

    plt.text(
        0, 
        y_values_ave[-4], 
        f"number of cmbs : N = {num_cmb} \n"+f"z $\in$ [{z_min},{z_max}] \n" + r"$\theta$ = "+f"{arcmin} arcmin"
    )

    plt.title("Vergleich Korrelation through und isw + random cmb (noise)")
    plt.legend(loc='lower right', fontsize='small')
    plt.xlabel(r'$\theta$ [arcmin]')
    plt.ylabel(r'Korrelation $\xi (\theta )$')

    fig.savefig(f"corr_with_error_comparation_{arcmin}_{z_min}_{z_max}_{percentile}.png")

    
    
    
    # gemittelt mit Fehler
    fig = plt.figure()
    plt.errorbar(
        x_values_ave,
        alpha * y_values_ave,
        yerr = np.abs(y_values_err),
        label='Korrelation through und isw + random cmb (noise)'
    )

    plt.text(
        0, 
        y_values_ave[-4], 
        f"number of cmbs : N = {num_cmb} \n"+f"z $\in$ [{z_min},{z_max}] \n" + r"$\theta$ = "+f"{arcmin} arcmin \n" + r"$f_{AHF} = $" + f"{np.round(alpha, 5)}"
    )

    plt.title("Korrelation through und isw + random cmb (noise)")
    plt.legend(loc='lower right', fontsize='small')
    plt.xlabel(r'$\theta$ [arcmin]')
    plt.ylabel(r'Korrelation $\xi (\theta )$')

    fig.savefig(f"corr_with_error_and_ahf_{arcmin}_{z_min}_{z_max}_{percentile}.png")

    
    
    
    for num in range(num_cmb):
        corr[num][3] = cov(corr[num][0],y_values_ave)

    save_corr(corr)
    
    
    
    # Bestimmen der gemitteleten Kovarianzmatrix
    cov_matrix_avg = np.zeros((20,20))

    for num in range(num_cmb):
        cov_matrix_avg += corr[num][3]
            
    cov_matrix_avg /= (num_cmb - 1)

    cov_matrix_avg_ahf = alpha * cov_matrix_avg
    
    
    
    
    # Plotten der Kovarianzmatrix
    plt.figure(figsize = (20,14))

    plt.rcParams.update({'font.size': 9})

    ax = sns.heatmap(
        cov_matrix_avg_ahf, 
        cmap = 'viridis', 
        annot=True, 
        square=True, 
        linewidths=.2,
        xticklabels = x_values_ave.round(decimals=1, out=None), 
        yticklabels = x_values_ave.round(decimals=1, out=None)
    )

    ax.invert_yaxis()
    plt.xlabel(r'$\theta$ [arcmin]')
    plt.ylabel(r'$\theta$ [arcmin]')
    plt.title("Kovarianz-Matrix - "+f"number of cmbs : N = {num_cmb}, "+f"z $\in$ [{z_min},{z_max}], " + r"$\theta$ = "+f"{arcmin} arcmin, " + r"$f_{AHF} = $"+ f"{np.round(alpha, 5)}")

    plt.savefig(f"cov_matrix_ahf_{arcmin}_{z_min}_{z_max}_{percentile}.png")
    plt.show()
    
    
    
    
    # Bestimmen der Korrelationsmatrix
    dia_stv_matrix = np.zeros((20,20))

    for i in range(20):
        dia_stv_matrix[i][i] = (y_values_err[i])
    
    dia_stv_matrix_inv = np.linalg.inv(dia_stv_matrix)

    corr_matrix_avg = np.matmul(dia_stv_matrix_inv, np.matmul(cov_matrix_avg, dia_stv_matrix_inv))
    
    
    
    
    
    # Plotten der Kovarianzmatrix
    plt.figure(figsize = (20,14))

    plt.rcParams.update({'font.size': 9})

    ax = sns.heatmap(
        corr_matrix_avg, 
        cmap = 'viridis', 
        annot=True, 
        square=True, 
        linewidths=.2,
        xticklabels = x_values_ave.round(decimals=1, out=None), 
        yticklabels = x_values_ave.round(decimals=1, out=None),
    )

    ax.invert_yaxis()
    plt.xlabel(r'$\theta$ [arcmin]')
    plt.ylabel(r'$\theta$ [arcmin]')
    plt.title("Korrelations-Matrix - "+f"number of cmbs : N = {num_cmb}, "+f"z $\in$ [{z_min},{z_max}], " + r"$\theta$ = "+f"{arcmin} arcmin")

    plt.savefig(f"corr_matrix_{arcmin}_{z_min}_{z_max}_{percentile}.png")
    plt.show()
    
    
    
    
    SNR_dict = {}

    # bereits vorhandene Daten laden
    if os.path.exists(f"snr_dict.txt"):
        SNR_dict = load_snr()  

    cov_matrix_avg_ahf_inv = np.linalg.inv(cov_matrix_avg_ahf)
    s = corr_without_noise[0]

    # Bestimmen des SNR-Werts
    #h = np.matmul(cov_matrix_avg_ahf_inv,s)/np.sqrt(np.matmul(s, np.matmul(cov_matrix_avg_ahf_inv,s)))
    #SNR = (np.matmul(h,s))/(np.sqrt(np.matmul(h,np.matmul(cov_matrix_avg_ahf, h))))
    SNR = np.sqrt(np.matmul(s,np.matmul(cov_matrix_avg_ahf_inv,s)))

    #h_single = [(cov_matrix_avg_ahf[i][i] * s[i])/np.sqrt((s[i] *(cov_matrix_avg_ahf[i][i]  * s[i]))) for i in range(20)]
    #SNR_sing = [((h_single[i] * s[i]))/(np.sqrt((h_single[i] * (cov_matrix_avg_ahf[i][i] * h_single[i])))) for i in range(20)]
    SNR_sing = [(np.sqrt(s[i] *(1/cov_matrix_avg_ahf[i,i])  *s[i])) for i in range(20)]

    SNR_dict[f"{arcmin}_{z_min}_{z_max}_{percentile}"] = [SNR, SNR_sing]

    save_snr(SNR_dict)
    
    print("finished")