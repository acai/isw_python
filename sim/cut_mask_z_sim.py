import numpy as np
import healpy as hp
import sys
from astropy.io import fits
from astropy.table import Table
import pandas as pd

## defining upper and lower bound for redshift
zmax = float(sys.argv[6])
zmin = float(sys.argv[5])

## selecting halos with mass in between 500 to 5000000 solar masses
dat1 = Table.read('sim_m_500_5000000.fits', format='fits')
dat1.keep_columns(['right_ascension', 'declination','redshift_observed'])

## converting the data from catalog to dataframe
df1 = dat1.to_pandas()
mask_zcut_max = sys.argv[7] #or '/home/u/Udit.Tyagi/help/mask_zcut_0.5.fits'#sys.argv[7] 

## converting dec to theta(healpy coordinate)
def theta(x):
    return np.pi / 2.0 - x * np.pi / 180.
df1['THETA'] = df1['declination'].apply(theta)

## converting ra to phi(healpy coordinate)
def phi(x):
    return x * np.pi / 180.
df1['PHI'] = df1['right_ascension'].apply(phi)

## def pixel
def pix(theta,phi):
    return hp.ang2pix(2048, theta, phi)
df1['PIX'] = df1.apply(lambda row: pix(row['THETA'], row['PHI']), axis=1)


## reading map
map = hp.read_map(mask_zcut_max, dtype=np.float64)

def p(pix):
    return map[pix]

df1['P'] = df1['PIX'].apply(p) 

## to filter out data according to redshift
condition = df1['P'] > 10**(-10)
filtered_df1 = df1[condition]
condition2 = (filtered_df1['redshift_observed'] >= zmin) & (filtered_df1['redshift_observed'] <= zmax)

filtered_df2 = filtered_df1[condition2]
filtered_df2.to_csv('output1.txt')

file_path = '/sim/output1.txt' ## give your own path 
file = open(file_path, 'r')

# Read and print the contents
file_contents = file.read()
