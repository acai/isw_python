import sys
import numpy as np
import healpy as hp
import pandas as pd

def main():
    if len(sys.argv) != 5:
        print("syntax:", sys.argv[0], "[healpix mask with redshift cut applied] [trough radius in arcmin] [output] [nside]")
        return 1

    thetat = float(sys.argv[2]) / 60. / 180. * np.pi

    print("// create healpix maps for mask")
    
    nside = int(sys.argv[4])
    mask = np.zeros(hp.nside2npix(nside))
     

    print("// read zmask")
    zmask = hp.read_map(sys.argv[1])
    num_rows = len(mask)
    
    for i in range(num_rows):
        theta, phi = hp.pix2ang(nside, i)
        listpix = hp.query_disc(nside, hp.ang2vec(theta, phi), thetat)
        
        m = 0
        for j in range(len(listpix)):
            m += zmask[listpix[j]]
        mask[i] = m/len(listpix)
   
        if np.random.randint(1000000) == 0:
            print("// of", len(listpix), "pixels the average was", mask[i])

    hp.write_map(sys.argv[3], mask, dtype=np.float32,overwrite = True)

    return 0

if __name__ == '__main__':
    main()


