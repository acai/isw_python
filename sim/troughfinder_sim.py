import numpy as np
from healpy import *
import time
import pandas as pd 
from multiprocessing import Pool, cpu_count #für Parallelisierung

start = time.time()

## chance the individual galaxy gets hit by a random mask 
def maskedcounts(count, mask, mthresh):
    d = np.random.binomial(count, 1. - mask + mthresh) 
    return d


def stat_index(count, countcut):
    idx = 0
    i = 0
    while i < len(countcut) and count > countcut[i]:
        idx += 2
        i += 1
    if i < len(countcut) and count == countcut[i]:
        idx += 1
    return idx

if __name__ == "__main__":
    import sys


    mthresh = 1. - float(sys.argv[3])
    print(mthresh)
    pcut = []
    for i in range(5, len(sys.argv)):
        pcut.append(float(sys.argv[i]))
        if len(pcut) > 1:
            assert pcut[-1] > pcut[-2] # make sure it's increasing at least a bit

    counts = read_map(sys.argv[1])
    mask = read_map(sys.argv[2])


    class Cylinder:
        def __init__(self, pix, counts, mask,maskedcounts):
            self.pix = pix
            self.counts = counts
            self.mask = mask
            self.maskedcounts = maskedcounts
    cylinders = []
    
    #Old Codelines START
    #s = 0 # pixels in survey
    
    #for i in range(mask.shape[0]):
        #m = mask[i]
        #if m > 0:
            #s += 1
        #if m < mthresh:
            #continue
            
        #c = counts[i]
        #mc = maskedcounts(c, m, mthresh)  # Calculate maskedcounts using the function
        #o = Cylinder(i, c, m, mc)
        #cylinders.append(o)
    #print("// accepted", len(cylinders), "/", s, "pixels in survey")
    #cylinders = sorted(cylinders, key=lambda x: x.maskedcounts)
    #Old Codelines START
    
    #Optimizing START
    s = np.count_nonzero(mask > 0)

    m_df = pd.DataFrame({'mask': mask})

    #m_df = m_df[(m_df['mask'] > 0) & (m_df['mask'] >= mthresh)]
    m_df = m_df[(m_df['mask'] >= mthresh)]
    index = np.array(m_df.index)

    m_count = counts[index]
    m_mask = mask[index]

    m_mc = np.random.binomial(m_count, 1. - m_mask + mthresh) 

    for x in range(len(m_mc)):
        cylinders.append(Cylinder(
            index[x],
            m_count[x],
            m_mask[x],
            m_mc[x]
        ))

    print("// accepted", len(cylinders), "/", s, "pixels in survey")

    cylinders = sorted(cylinders, key=lambda x: x.maskedcounts)
    #Optimizing END


    countcut = []
    for p in pcut:
        countcut.append(cylinders[int(p * len(cylinders))].maskedcounts)
        
        print("// percentile", p, "corresponds to count", countcut[-1])


    pcut.append(1.0)
    cylinders = sorted(cylinders, key=lambda x: x.pix)
    stats = np.zeros((len(cylinders), 2*len(countcut)+1), dtype=int)
    ntrials = 100

    print("// running trials", end=" ")
    
    #Old Codelines START
    #for i in range(ntrials):
        #for j in range(len(cylinders)):
            #stats[j][stat_index(cylinders[j].maskedcounts, countcut)] += 1
          
        #if i < (ntrials - 1):
            #for j in range(len(cylinders)):
                #cylinders[j].maskedcounts = maskedcounts(cylinders[j].counts, cylinders[j].mask, mthresh)

        #print(".", end=" ")

    #print("done.")
    #Old Codelines END
    
    #Optimizing START(Parallelisierung)
    def task(cylinders) : #, countcut, ntrials):
        stats = np.zeros((len(cylinders), 2*len(countcut)+1), dtype=int)
        for j in range(len(cylinders)):
            for i in range(ntrials):
                stats[j][stat_index(cylinders[j].maskedcounts, countcut)] += 1
          
                if i < (ntrials - 1):
                    cylinders[j].maskedcounts = maskedcounts(cylinders[j].counts, cylinders[j].mask, mthresh)
        return stats
    
    cores = cpu_count()
    print("number of cores :",cores)
    
    cylinders_split = np.array_split(cylinders, cores, axis=0)
    
    with Pool() as pool:
        cylinders_total = pool.map(task, cylinders_split)
    
    stats = np.concatenate(cylinders_total, axis=0)
    print("done.")
    #Optimizing END
    
    #Old Codelines START
    #cbin = np.zeros(2*len(countcut)+1, dtype=int)
    #pbin = np.zeros(2*len(countcut)+1, dtype=float)

    #for i in range(2*len(countcut)+1):
        #cbin[i] = 0
        #for j in range(len(cylinders)):
                       #cbin[i]+=stats[j][i]           
        #pbin[i] = cbin[i] / float(ntrials) / float(len(cylinders))
    #Old Codelines END
    
    #Optimizing START
    cbin = np.sum(stats, axis=0)
    pbin = cbin/ float(ntrials) / float(len(cylinders))
    #Optimizing END

    psum = 0.
    nsum = 0
    for i in range(2*len(countcut)+1):
        print("// pbin[", i, "]=", pbin[i])
        psum += pbin[i]
        nsum += cbin[i]

    print("// sum(pbin)=", psum, "sum(cbin)=", nsum, "ntrials=", float(ntrials)*float(len(cylinders)))

    wlimit_used = 0.
    pbelow = 0.

    for i in range(len(countcut)+1):
        idx1 = i * 2
        idx2 = idx1 + 1
        idx0 = idx1 - 1

        wbin = np.zeros(3)
        wbin[1] = 1.
        wbin[0] = 1. - wlimit_used

        if idx2 < (2 * len(countcut) + 1):
            wbin[2] = ((pcut[i] - pbelow) - wbin[1] * (pbin[idx1])) / pbin[idx2] 
            if idx0 >= 0:
                wbin[2] -= wbin[0] * pbin[idx0]/ pbin[idx2] 
        else:
            wbin[2] = 0.

        print("// percentile range", i, "has wbin[0,1,2]=", wbin[0], ",", wbin[1], ",", wbin[2])

        map = np.zeros_like(mask)
        wsum = 0.

        if i == 0:
            for j in range(len(cylinders)):
                map[cylinders[j].pix] = (wbin[1] * float(stats[j][idx1]) + wbin[2] * float(stats[j][idx2])) / float(ntrials)
                wsum += map[cylinders[j].pix]
        elif i == len(countcut):
            for j in range(len(cylinders)):
                map[cylinders[j].pix] = (wbin[1] * float(stats[j][idx1]) + wbin[0] * float(stats[j][idx0])) / float(ntrials)
                wsum += map[cylinders[j].pix]
        else:
            for j in range(len(cylinders)):
                map[cylinders[j].pix] = (wbin[1] * float(stats[j][idx1]) + wbin[0] * float(stats[j][idx0]) + wbin[2] * float(stats[j][idx2])) / float(ntrials)
                wsum += map[cylinders[j].pix]

        write_map(sys.argv[4] + "_" + str(i) + ".fits", map, overwrite=True)

        wlimit_used = wbin[2]
        print("// percentile range", i, "has total p=", wsum / float(len(cylinders)), "where it should be", pcut[i] - pbelow)
        pbelow = pcut[i]

    troughmask = np.zeros_like(mask)

    for j in range(len(cylinders)):
        troughmask[cylinders[j].pix] = 1.

    write_map(sys.argv[4] + "_troughmasrepresents an angle of 10 arc minutesk.fits", troughmask, overwrite=True)

end = time.time()
print(end - start)