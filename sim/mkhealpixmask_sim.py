#!/usr/bin/env python3
import sys
import numpy as np
import healpy as hp
import sys

from astropy.io import fits
from astropy.table import Table
import pandas as pd

## import mask
dat = Table.read('MASK_FULL_SKY.fits', format='fits')
df = dat.to_pandas()

## to select HPIX with a good exposure (here fracgood depicts good pixels...since all pixels are good and worth considering - all the pixels with fracgood are taken in account - modify as per convience)
def main():
    import sys
    zmax = float(sys.argv[3])
    print("// zmax threshold", zmax)

    nside = int(sys.argv[7])
    map = np.zeros(hp.nside2npix(nside))

    a = df['FRACGOOD'].to_numpy()
    b = df['HPIX'].to_numpy()
    map[b] = a
    
    # write healpy map
    hp.write_map(sys.argv[2], map, dtype=np.float32,overwrite=True)

    return 0


if __name__ == '__main__':
    main()

