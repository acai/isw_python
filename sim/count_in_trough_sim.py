import numpy as np
import healpy as hp
import sys
import pandas as pd


if len(sys.argv) < 4:
    print("syntax:", sys.argv[0], "[tracer input catalog with 1=RA, 2=dec, redshift cut applied]",
          "[trough radius in arcmin]", "[output] (Nside)")
    sys.exit(1)

# radius of circle in arcmin
thetat = float(sys.argv[2]) / 60. / 180. * np.pi  

print("// read tracer catalog")

## importing tracer_text_file as dataframe
df2 = pd.read_csv(sys.argv[1])

nside = 2048

if len(sys.argv) > 4:
    rank = int(sys.argv[4])
count = np.zeros(hp.nside2npix(nside), dtype=np.int32)

## disc around the tracer pixels (for counting number of galaxies in discs)

if thetat>0:    
    def pixel_indices(nside,theta,phi,thetat):
        return hp.query_disc(nside, hp.ang2vec(theta, phi), thetat)
    df2['pixel_indices'] = df2.apply(lambda row: pixel_indices(nside,row['THETA'], row['PHI'],thetat), axis=1)
    pixel_indices = df2['pixel_indices'].to_numpy()
    pixel = df2['PIX'].to_numpy()
    df2['pixel_indices'] = df2['pixel_indices'].apply(lambda x: [x] if isinstance(x, int) else x)


else:    
    def pixel_indices(nside,theta,phi):
        return hp.ang2pix(nside, theta, phi)
    df2['pixel_indices'] = df2.apply(lambda row: pixel_indices(nside,row['THETA'], row['PHI']), axis=1)
    pixel_indices = df2['pixel_indices'].to_numpy()
    pixel = df2['PIX'].to_numpy()
    df2['pixel_indices'] = df2['pixel_indices'].apply(lambda x: [x] if isinstance(x, int) else x)

pix_array = np.concatenate(df2['pixel_indices'].values)
    
## actual counting
counts = np.bincount(pix_array)
count[:len(counts)] = counts

## write healpy map
hp.write_map(sys.argv[3], count, overwrite=True)

