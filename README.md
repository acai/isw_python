1. First step is to plot trough maps using the python and bash file provided under the sim directory.

2. Start by running the bash file (define appropriate path for calling out python files wherever required)(Note - The code is computationally heavy and takes a lot of time to run - try to boost the performance by reducing the run time if possible)

3. Before running the bash file you have to make a mask healpix map for the entire sky. (Its quite easy - Just set all the healpix pixels to one)

4. Once done with this next step is to get a cross correlation between trough maps and isw maps using treecorr package. ISW maps are already provided.(Use ISW_theory+calculated for assistance)

5. Final step is to match the results with theoritical ISW predictions.(Also in Cross - correlation directory)
